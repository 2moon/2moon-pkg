#!/usr/bin/env node
require('open')('https://two-moon.now.sh', {
  wait: false
})

const words = [
	'🌕',
	'🌑',
	'🌗',
	'🌓',
	'🌒',
	'🌔',
	'🌒',
	'🌛',
	'🌜',
	'🌚',
	'🌝',
	'🌙',
]

console.log(words[Math.floor(Math.random()*words.length)])
